package com.codingraja.domain;

public class Product {
	private Long id;
	private String name;
	private String model;
	private String brand;
	private Double price;
	
	public Product(){}
	
	public Product(String name, String model, String brand, Double price) {
		super();
		this.name = name;
		this.model = model;
		this.brand = brand;
		this.price = price;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", model=" + model + ", brand=" + brand + ", price=" + price
				+ "]";
	}
}
