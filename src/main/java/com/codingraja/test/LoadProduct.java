package com.codingraja.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Product;

public class LoadProduct {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration().configure("hibernate.cfg.xml");
		SessionFactory factory = configuration.buildSessionFactory();
		
		Session session = factory.openSession();
		Product product = session.load(Product.class, new Long(1));
		System.out.println("Brand Name: "+product.getBrand());
		session.close(); 
	}

}
