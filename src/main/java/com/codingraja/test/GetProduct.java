package com.codingraja.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Product;

public class GetProduct {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration().configure("hibernate.cfg.xml");
		SessionFactory factory = configuration.buildSessionFactory();
		
		Session session = factory.openSession();
		Product product = session.get(Product.class, new Long(1));
		session.close();
		System.out.println("Product Details: "+product);
	}

}
