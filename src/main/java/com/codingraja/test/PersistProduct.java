package com.codingraja.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Product;

public class PersistProduct {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration().configure("hibernate.cfg.xml");
		SessionFactory factory = configuration.buildSessionFactory();
		
		Product product = new Product("Mac","MacBook Air", "Apple", 65000.00);
		
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		session.persist(product);
		transaction.commit();
		session.close();
		System.out.println("Product has been Saved Successfully");
	}

}
