package com.codingraja.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Product;

public class SaveProduct {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration().configure("hibernate.cfg.xml");
		SessionFactory factory = configuration.buildSessionFactory();
		
		//Create Product Object
		Product product = new Product("Mac","MacBook Pro", "Apple", 75000.00);
		
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		long id = (Long)session.save(product);
		transaction.commit();
		session.close();
		System.out.println("Product Id: "+id);
	}

}
